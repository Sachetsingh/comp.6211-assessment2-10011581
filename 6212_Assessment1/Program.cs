﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;

namespace _6212_Assessment2
{
    class Program
    {   
        static public string choice;
        static public double[] Top = new double[10];
        static public double[] arrMain = new double[0];
        static public string[] arrDaT = new string[0];
        static public int QN;
        static public double Maxi;
        static public int[] MaxiIndices;
        static public double Mini;
        static public int[] MiniIndices;
        static public double Avar;
        static public int[] AvarIndices;
        static void Main(string[] args)
        {
            do
            {
                // Menu description for complete program
                Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                Console.WriteLine("~~~~~~~~~   Choose any number for diffrent programs :-  ~~~~~~~~~");
                Console.WriteLine("~~~~~~~~~   1.Finding the top 10 value of a text file   ~~~~~~~~~");
                Console.WriteLine("~~~~~~~~~   2.Selection sort                            ~~~~~~~~~");
                Console.WriteLine("~~~~~~~~~   3.Linear Search                             ~~~~~~~~~");
                Console.WriteLine("~~~~~~~~~   4.Bubble sort                               ~~~~~~~~~");
                Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                var read = Console.ReadLine();
                if (read == "1")
                {
                    Console.Clear();
                    importData();
                    printArray(arrMain);
                    Console.WriteLine("Top 10 values of the text file:\n");
                    Top = findMaximum();
                    QN = 1; // refers to question 1 - for finding top 10 values
                    printArray(Top);
                    Console.WriteLine("For repeat the program, 'y' for yes, 'n' for no");
                    choice = Console.ReadLine();
                    Console.Clear();


                }
                else if (read == "2")
                {
                    Console.Clear();
                    QN = 2; // refers to question 2 - selection sort
                    importData();
                    Console.WriteLine("Unsorted:\n");
                    printArray(arrMain);
                    Console.WriteLine("Press 'Enter' to view sorted File!");
                    Console.ReadLine();
                    Console.WriteLine("Sorted:\n");
                    double[] sortedArr = selectionSort(arrMain);
                    printArray(sortedArr); //Calls the selection sort algorithm
                    Console.WriteLine("Press 'Enter' to view maximum, minimum and average of the values!");
                    Console.ReadLine();

                    Maxi = sortedArr[sortedArr.Length - 1];
                    Mini = sortedArr[0];
                    Avar = 0;

                    for (int i = 0; i < sortedArr.Length; i++) // method used by 'for' loop
                    {
                        Avar += sortedArr[i];
                    }

                    Avar = Avar / sortedArr.Length;

                    FindValues();
                    Console.WriteLine();
                    Console.WriteLine("For repeat the program, 'y' for yes, 'n' for no");
                    choice = Console.ReadLine();
                    Console.Clear();
                }
                else if (read == "3")
                {
                    Console.Clear();
                    QN = 3; // refers to question 3 - Linear Search
                    importData();
                    linearSearch();
                    Console.WriteLine("Press 'Enter' to see time of maximum, minimum and average of the values!");
                    Console.ReadLine();
                    compareArrays();
                    Console.WriteLine("\nFor repeat the program, 'y' for yes, 'n' for no");
                    choice = Console.ReadLine();
                    Console.Clear();
                }
                else if (read == "4") 
                {
                    Console.Clear();
                    QN = 4; // refers to question 4 - Bubble sort
                    BubbleSortTimer bubbleSorting = new BubbleSortTimer();
                    bubbleSorting.bubbleSort();
                    bubbleSorting.improvedBubbleSort();
                    Console.WriteLine("\nFor repeat the program, 'y' for yes, 'n' for no");
                    choice = Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Command again!");
                    choice = "y";
                }
            } while (choice == "y"); // method used by 'while' loop

            if (choice == "n")
            {
                Console.WriteLine("~~~~~~~   Thank you   ~~~~~~~");
            }
            else
            {
                Console.WriteLine("Restart the program");
            }
            Console.Clear();

        }
        static public void importData() 
        {
            double temp = 0; // temporarily used to store value
            int counter = 0;
            string line;

            arrMain = null; // taking there value as null
            arrDaT = null;

            StreamReader file = new StreamReader(@"c:\Moisture_Data.txt");
            while ((line = file.ReadLine()) != null) // method used by 'while' loop
            {
                double.TryParse(line, out temp);
                Array.Resize(ref arrMain, counter + 1);
                arrMain[counter] = temp;
                counter++;
            }
            file.Close();

            if (QN == 3) // user make choice for question 3
            {
                StreamReader file2 = new StreamReader(@"c:\DateTime_Data.txt");
                counter = 0;
                while ((line = file2.ReadLine()) != null) // method used by 'while' loop
                {
                    Array.Resize(ref arrDaT, counter + 1);
                    arrDaT[counter] = line;
                    counter++;
                }
                file2.Close();
            }
        }
        static public double[] findMaximum() // making input for finding maximum
        {
            double[] ArrTemp = new double[arrMain.Length];
            Array.Copy(arrMain, ArrTemp, arrMain.Length);

            double[] maxArray = new double[10];
            int maxIndex = 0;

            for (int i = 0; i < 10; i++) // loops goes through all of the objects in the array
            {
                maxArray[i] = ArrTemp[0];

                for (int j = 1; j < ArrTemp.Length; j++) //Inner loop goes thorough and does the swaps
                {
                    if (maxArray[i] < ArrTemp[j]) // checking conditions
                    {
                        maxArray[i] = ArrTemp[j];
                        maxIndex = j;
                    }
                }

                ArrTemp[maxIndex] = 0;
            }

            return maxArray; // return the array
        }

        static public void printArray(double[] tempArr)
        {
            for (int i = 1; i <= tempArr.Length; i++)
            {
                Console.Write(tempArr[i - 1] + ",  ");
                if (i % 10 == 0)                        //will only display 10 value per line.
                    Console.WriteLine();
            }

            Console.WriteLine();
        }

        public static double[] selectionSort(double[] data)
        {
            int max;
            double temp;

            for (int i = 0; i < data.Length - 1; i++) // loops goes through all of the objects in the array
            {
                max = i;
                for (int j = i + 1; j < data.Length; j++) //Inner loop goes thorough and does the swaps
                {
                    if (data[j] > data[max]) // checking conditions
                    {
                        max = j;
                    }
                }

                if (max != 1) // checking conditions
                {
                    temp = data[i];
                    data[i] = data[max];
                    data[max] = temp;
                }
            }

            return data; // return
        }

        static public void FindValues()
        {
            Console.WriteLine("Max:   " + Maxi);
            Console.WriteLine("Min:   " + Mini);
            Console.WriteLine("Avar:  " + Math.Round(Avar,2));

        }
        static public void linearSearch()
        {
            MaxiIndices = null;
            MiniIndices = null;
            AvarIndices = null;

            int count = 0;
            Console.WriteLine("Indice of the maximum values are: ");
            for (int i = 0; i < arrMain.Length; i++) // method used by 'for' loop
            {
                if (arrMain[i] == Maxi) // checking conditions
                {
                    Console.Write(i + " ");
                    Array.Resize(ref MaxiIndices, count + 1);
                    MaxiIndices[count] = i;
                    ++count;
                }
            }
            Console.WriteLine();
            Console.WriteLine("Indices of the minimum values are: ");

            count = 0;
            for (int i = 0; i < arrMain.Length; i++)
            {
                if (arrMain[i] == Mini) // checking conditions
                {
                    Console.Write(i + " ");
                    Array.Resize(ref MiniIndices, count + 1);
                    MiniIndices[count] = i;
                    ++count;
                }
            }
            Console.WriteLine();
            Console.WriteLine("Indices of the avarage values are: ");

            count = 0;
            for (int i = 0; i < arrMain.Length; i++) // method used by 'for' loop
            {
                if (arrMain[i] == Avar) // checking conditions
                {
                    Console.Write(i + " ");
                    Array.Resize(ref AvarIndices, count + 1);
                    AvarIndices[count] = i;
                    ++count;
                }
            }

            if (AvarIndices == null) // taking value as null
            {
                Console.WriteLine("No data");
            }
        }
        static public void compareArrays()
        {
            Console.WriteLine("Occurrence of maximum value: ");
            Console.WriteLine(MaxiIndices.Length);
            for (int i = 0; i < MaxiIndices.Length; i++)
            {
                Console.WriteLine(arrDaT[MaxiIndices[i]]);
            }

            Console.WriteLine();
            Console.WriteLine("Occurrence of minimum value: ");

            for (int i = 0; i < MiniIndices.Length; i++)
            {
                Console.WriteLine(arrDaT[MiniIndices[i]]); // display content for indices
            }

            Console.WriteLine("Occurrence of avarage value: ");
            Console.WriteLine();

            if (AvarIndices != null) // checking conditions
            {
                for (int i = 0; i < AvarIndices.Length; i++) // method used by 'for' loop
                {
                    Console.WriteLine(arrDaT[AvarIndices[i]]);
                }
            }
        }
    }
    class BubbleSortTimer
    {
        public double temp = 0;
        public int counter = 0;
        public string line;
        public double[] arr = new double[0];
        Stopwatch st = new Stopwatch();
        public void bubbleSort()
        {

            StreamReader file = new StreamReader(@"c:\Moisture_Data.txt");
            while ((line = file.ReadLine()) != null) // method used by 'while' loop
            {
                double.TryParse(line, out temp);
                Array.Resize(ref arr, counter + 1);
                arr[counter] = temp;
                counter++;
            }
            file.Close();
            st.Start();
            double[] NorArr = new double[arr.Length];
            Array.Copy(arr, NorArr, arr.Length);
            for (int write = 0; write < NorArr.Length; write++) // loops goes through all of the objects in the array.
            {
                for (int sort = 0; sort < NorArr.Length - 1; sort++) //Inner loop goes thorough and does the swaps
                {
                    if (NorArr[sort] > NorArr[sort + 1]) // checking conditions
                    {
                        temp = NorArr[sort + 1];
                        NorArr[sort + 1] = NorArr[sort];
                        NorArr[sort] = temp;
                    }
                }
            }
            st.Stop();
            Program.printArray(NorArr);
            Console.WriteLine("Bubble sort as normal: {0}", st.Elapsed);
            Console.WriteLine();
        }
        public void improvedBubbleSort()
        {
            double[] Imp1Arr = new double[arr.Length];
            Array.Copy(arr, Imp1Arr, arr.Length);
            double[] Imp2Arr = new double[arr.Length];
            Array.Copy(arr, Imp2Arr, arr.Length);
            st.Reset();
            st.Start();
            for (int write = 0; write < Imp1Arr.Length; write++) //loops goes through all of the objects in the array
            {
                int test = 0;

                for (int sort = 0; sort < Imp1Arr.Length - 1 - write; sort++) //Inner loop goes thorough and does the swaps
                {
                    if (Imp1Arr[sort] > Imp1Arr[sort + 1]) // checking conditions
                    {
                        temp = Imp1Arr[sort + 1];
                        Imp1Arr[sort + 1] = Imp1Arr[sort];
                        Imp1Arr[sort] = temp;
                        test = 1;
                    }
                }
                if (test == 0)
                {
                    break;
                }
            }
            st.Stop();
            Program.printArray(Imp1Arr);
            Console.WriteLine("\nBubble sort as improved takes: {0}", st.Elapsed);

        }
    }

}

